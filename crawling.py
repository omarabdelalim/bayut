from pymongo import MongoClient
def connect_cluster(db_name,collection_name):
 
   # Provide the mongodb atlas url to connect python to mongodb using pymongo
   CONNECTION_STRING = "mongodb+srv://o_abdelalim:ProperBird@bayut-dubai.cmtr77d.mongodb.net/?retryWrites=true&w=majority"
 
   # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
   client = MongoClient(CONNECTION_STRING)
   db = client.get_database(db_name)
   collection = db.get_collection(collection_name)
   return collection

def connect_db(db_name):
   CONNECTION_STRING = "mongodb+srv://o_abdelalim:ProperBird@bayut-dubai.cmtr77d.mongodb.net/?retryWrites=true&w=majority"
 
   # Create a connection using MongoClient. You can import MongoClient or use pymongo.MongoClient
   client = MongoClient(CONNECTION_STRING)
   db = client.get_database(db_name)
   return db

database = connect_db("Bayut-Dubai")
collection =database.get_collection("allListings")

cursor = collection.find()

## so I personally think the focus should mostly be on the location then the other criteria
unique = []
 
locations = []
area_rooms_baths = {}

for i in cursor:
   curr_loc = ""
   #assuming all the levels should be the same, ie if some property has a location with 4 levels and shares the first 3
   #with a 3 level property, they aren't the same
   for j in i["location"]:
      curr_loc+= ("\t" + j["name"])
   if curr_loc in locations:
      arb = [i["area"],i["rooms"],i["baths"]]
      if not curr_loc in area_rooms_baths:
         area_rooms_baths[curr_loc] = [arb]
         unique.append(i)
      elif not (arb in area_rooms_baths[curr_loc]):
         area_rooms_baths[curr_loc].append(arb)
         unique.append(i)
   else:
      locations.append(curr_loc)
      unique.append(i)
      area_rooms_baths[curr_loc] = [i["area"],i["rooms"],i["baths"]]
         


      
if not("uniqueListings" in database.list_collection_names()):
   coll = database["uniqueListings"]
   coll.insert_many(unique)




            

